# Installation Guide
Video Guide: https://www.youtube.com/watch?v=K4zSRENyjQ0
- Download this directory as a .zip file
- drag the Data folder from the .zip file into your `C:\Users\<USER>\AppData\Local\FoundryVTT` directory
- Install the `Sandbox Extensions, Custom CSS, _CodeMirror, libWrapper, & Compendium Folders` modules
- Create a new world, set the `Game System` to `Sandbox System Builder`
- Log in to the new world as a GM, and enable all of the above modules, as well as the `Sword World 2.0 Sandbox Game System` compendium module
- Go to the compendium tab, open and import all of the folders within `_ITEM_TEMPLATES` first (important)
- Then import the folder within `_ACTOR_TEMPLATES` compendium
- Open the `_templateNPC` actor, toggle the `Template` checkbox to `True` and then refresh the template-actor
- Repeat this step for `_templatePC`
- Import the `Power Table` from the Journal Entries compendium
- Now, you're basically done installing the system.

# Settings
- System Settings (Sandbox): I typically disable all of the checkboxes (advantage icon, DC window, etc.), and click SAVE
- Sandbox Extensions (Module): write `_icon` into the "table key suffix" field near the top of the settings list, and click SAVE
- Custom CSS (Module): Copy the contents of `css-style.css` located in this directory into the editor window, and click SAVE

# Update Guide
Video Guide: https://www.youtube.com/watch?v=yobuAf_fD74
- Same steps as above, except you need to overwrite the exist system/module
- Log into your existing World
- DELETE-ALL on the RED `_TEMPLATE` folders
- Import those folder structures again
- Toggle / Rebuild the `_templateActors` (PC & NPC)
- Open each Actor you have (player sheets, or monsters) and click the DOWN ARROW to refresh them
##### These two steps MAY not be necessary, I'll have to do some testing.
- Delete any existing items, and remove them from actors
- Re-import and re-add them to the actors.

# Known Issues
- ~~Sandbox currently has an issue with compendiums enabled that include Sandbox-elements (So, everything...). This is a problem because it will add 2-5sec (minimum, could be worse for you) to ANY interfacing with actors (PC/NPC health changes, stat update, adding new items, EVERYTHING becomes slower for actors). **The work around is to DISABLE the `Sword World 2.0 Sandbox Game System` compendium module after you are done importing just the items you need.** You can always re-enable it later.~~ This has been fixed in the most recent update.
- About Compendiums & Foundry: Foundry VTT loads all items, actors, and scenes into memory and sends them though the network to all of your players (I am paraphrasing, but this is basically what happens). This is fine if you have a hundred, or two or three, total items. But this has a serious impact on performance for all users who do not have great RAM, CPU, or network speeds. So, do NOT import everything from the compendiums. Anything in a compendium is not loaded, and only creates the legitimate items when they're needed or imported.
